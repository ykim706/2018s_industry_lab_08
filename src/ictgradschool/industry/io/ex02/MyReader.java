package ictgradschool.industry.io.ex02;

import ictgradschool.Keyboard;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class MyReader {

    public void start() {
        System.out.println("Enter a file name: ");
        String fileName = Keyboard.readInput();
        File myFile = new File(fileName);
        try
                (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }

        } catch (IOException e) {
            System.out.println("IO problem");
        }
    }

    public static void main(String[] args) {
        new MyReader().start();
    }
}


// TODO Prompt the user for a file name, then read and print out all the text in that file.
// TODO Use a BufferedReader.