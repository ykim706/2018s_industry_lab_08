package ictgradschool.industry.io.ex01;

import com.sun.org.apache.xpath.internal.SourceTree;

import java.io.*;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;
        int num;

        FileReader fR = null;
        try {
            fR = new FileReader("input2.txt");
            while ((num = fR.read()) != -1) {
                total++;
                char character = (char) num;
                if (character == 'e' || character == 'E') {
                    numE++;
                }

            }

        } catch (FileNotFoundException e) {
            System.out.println("File not found problem" + e);
        } catch (IOException e) {
            System.out.println("IO problem");
        }

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;
        String read;

        File newFile = new File("input2.txt");
        try
                (BufferedReader reader = new BufferedReader(new FileReader(newFile))) {
            while ((read = reader.readLine()) != null) {
                for (int i = 0; i < read.length(); i++) {
                    total++;
                    if (read.charAt(i) == 'e') {
                        numE++;
                    }
                    if (read.charAt(i) == 'E') {
                        numE++;
                    }
                }
            }


        } catch (IOException e) {
            System.out.println("IO problem");
        }

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
